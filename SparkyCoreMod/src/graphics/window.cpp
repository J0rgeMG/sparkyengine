#include "window.h"

namespace sparky {
	namespace graphics {

		void windowResize(GLFWwindow* window, int width, int height);

		Window::Window(const char* title, int width, int height) {
			m_Title = title;
			m_Width = width;
			m_Height = height;

			if (!init()) {
				glfwTerminate();
			}
		}

		Window::~Window() {
			glfwTerminate();
		}

		void Window::clear() const {
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		}

		void Window::update() const {
			glfwPollEvents();
			glfwSwapBuffers(m_Window);
		}

		bool Window::init() {
			// Init GLFW
			if (!glfwInit()) {
				std::cout << "Init GLFW error..." << std::endl;
				return false;
			}

			// Init Window
			m_Window = glfwCreateWindow(m_Width, m_Height, m_Title, NULL, NULL);

			// If not Init Window we say it
			if (!m_Window) {
				std::cout << "Failed to create the GLFW Window (m_Window)" << std::endl;
				return false;
			}

			// Make our Window the current context
			glfwMakeContextCurrent(m_Window);
			glfwSetWindowSizeCallback(m_Window, windowResize);

			// Init GLEW. It's important that it be after declaring the context
			if (glewInit() != GLEW_OK) {
				std::cout << "Failed to initialize GLEW" << std::endl;
				return false;
			}

			return true;
		}

		bool Window::closed() const {
			return glfwWindowShouldClose(m_Window);
		}

		void windowResize(GLFWwindow* window, int width, int height) {
			glViewport(0, 0, width, height);
		}
	}
}