#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>

namespace sparky {
	namespace graphics {

		class Window {
		private:
			const char* m_Title;
			int m_Width, m_Height;
			GLFWwindow* m_Window;
		public:
			Window(const char* title, int width, int height);
			~Window();
			void clear() const;
			void update() const;
			bool closed() const;
		private:
			bool init();
		};

	}
}